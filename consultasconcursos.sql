﻿USE concursos;
-- indicar el nombre de los concursantes de burgos. con like indica los caracteres y no distingue entre mayusculas y minusculas pero va mas lento
  SELECT DISTINCT c.nombre FROM concursantes c WHERE c.provincia LIKE '%burgos%'; 

  -- indicar cuantos concursantes hay de burgos
    SELECT COUNT(*) FROM concursantes c WHERE c.provincia= 'Burgos';
    
   -- indicar cuantos concursantes hay de cada poblacion
    SELECT COUNT(*) concursantes, c.poblacion FROM concursantes c GROUP BY c.poblacion; 
    
   -- indicar las poblaciones que tienen concursantes de menos de 90 kilos
    SELECT DISTINCT c.poblacion FROM concursantes c WHERE c.peso<90; 
    
   -- indicar las poblaciones que tienen mas de 1 concursante
    SELECT c.poblacion FROM concursantes c GROUP BY c.poblacion HAVING COUNT(*)>1;     
    
    -- indicar las poblaciones que tienen mas de 1 concursante de menos de 90 kilos 
      SELECT c.poblacion FROM concursantes c WHERE c.peso<90 GROUP BY c.poblacion HAVING COUNT(*)>1;      

      SELECT c.poblacion FROM concursantes c WHERE c.peso>90 AND c.poblacion LIKE "L%";